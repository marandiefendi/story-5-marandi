from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name = 'home'),
    path('exped/', views.exped, name = 'exped'),
    path('skills/', views.skills, name = 'skills'),
    path('contact/', views.contact, name = 'contact'),
]
 