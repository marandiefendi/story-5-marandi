# Generated by Django 2.2.5 on 2019-10-07 11:21

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('time', models.TimeField()),
                ('activity', models.CharField(max_length=30)),
                ('place', models.CharField(max_length=20)),
                ('category', models.CharField(max_length=20)),
            ],
        ),
    ]
